import React, { Component } from "react";
import { SVGArrowDown } from "../assets/icons";
import ProductCard from "../components/ProductCard";

class Products extends Component {
  renderAside = () => {
    return <div className="col-md-3">
    {this.renderFiltered()}
    {this.renderFilterByColor()}
    {this.renderFilterByPrice()}
    </div>;
  };

  renderFiltered = () => {
    const styles = [
      {
        color: "#FFF",
        backgroundColor: "#8A2454"
      },
      {
        color: "#FFF",
        backgroundColor: "#475984"
      },
      {
        color: "#FFF",
        backgroundColor: "#BF6989"
      },
      {
        color: "#FFF",
        backgroundColor: "#9A54D8"
      }
    ];
    return (
      <div className="aside">
        <h3 className="aside-title">Shop by:</h3>
        <ul className="filter-list">
          <li>
            <span className="text-uppercase">color:</span>
          </li>
          <li>
            <a href="/url" style={styles[0]}>
              Camelot
            </a>
          </li>
          <li>
            <a href="/url" style={styles[1]}>
              Camelot
            </a>
          </li>
          <li>
            <a href="/url" style={styles[2]}>
              Camelot
            </a>
          </li>
          <li>
            <a href="/url" style={styles[3]}>
              Camelot
            </a>
          </li>
        </ul>
      </div>
    );
  };

  renderFilterByColor = () => {
    const colors = [
      '#475984', '#8A2454', '#BF6989', '#9A54D8', '#675F52', '#050505', '#D5B47B'
    ]
    return (
     
      <div className="aside">
        <h3 className="aside-title">Filter By Color:</h3>
        <ul className="color-option">
          <li>
            <a href="/url" style={{background: colors[0]}} />
          </li>
          <li>
            <a href="/url" style={{background: colors[1]}} />
          </li>
          <li className="active">
            <a href="/url" style={{background: colors[2]}} />
          </li>
          <li>
            <a href="/url" style={{background: colors[3]}} />
          </li>
          <li>
            <a href="/url" style={{background: colors[4]}} />
          </li>
          <li>
            <a href="/url" style={{background: colors[5]}} />
          </li>
          <li>
            <a href="/url" style={{background: colors[6]}} />
          </li>
        </ul>
      </div>
    );
  };

  renderFilter = () => {
    return (
      <div>
        {/* <div classNameName="row-filter">
          <a href="#">
            <i className="fa fa-th-large" />
          </a>
          <a href="#" className="active">
            <i className="fa fa-bars" />
          </a>
        </div> */}
        <div className="sort-filter d-flex align-items-center">
          <span className="text-uppercase">Sort By:</span>
          <select className="input">
            <option value="0">Position</option>
            <option value="0">Price</option>
            <option value="0">Rating</option>
          </select>
          <a href="#" className="main-btn icon-btn">
            <SVGArrowDown direction="down" size="14" />
          </a>
        </div>
      </div>
    );
  };

  renderFilterByPrice = () => {
    return (
      <div className="aside">
        <h3 className="aside-title">Filter By Size:</h3>
        <ul className="size-option">
          <li className="active"><a href="#">S</a></li>
          <li className="active"><a href="#">XL</a></li>
          <li><a href="#">SL</a></li>
        </ul>
      </div>
    )
  }

  renderPaging = () => {
    return (
      <div>
        <div className="page-filter">
          <span className="text-uppercase">Show:</span>
          <select className="input">
            <option value="0">10</option>
            <option value="1">20</option>
            <option value="2">30</option>
          </select>
        </div>
        <ul className="store-pages">
          <li>
            <span className="text-uppercase">Page:</span>
          </li>
          <li className="active">1</li>
          <li>
            <a href="#">2</a>
          </li>
          <li>
            <a href="#">3</a>
          </li>
          <li>
            <a href="#">
              <i className="fa fa-caret-right" />
            </a>
          </li>
        </ul>
      </div>
    );
  };

  renderMain = () => {
    return (
      <div className="col-md-9">
        <div className="store-filter clearfix">
          <div className="d-flex justify-content-between">
            {this.renderFilter()}
            {this.renderPaging()}
          </div>
          <div className="row">{this.renderListProduct()}</div>
        </div>
      </div>
    );
  };

  renderListProduct = () => {
    return (
      <>
        {Array.from(Array(7)).map(i => {
          return (
            <div class="col-md-4 col-sm-6 col-xs-6">
              <ProductCard
                isNew={true}
                discountPercent="30"
                countdownEnd="2019-06-25T10:13:17.800Z"
                productImg="https://i.ibb.co/nCNntgJ/product02.jpg"
                discountPrice="32.50"
                originalPrice="45.00"
                productUrl="/home"
                productName="Tui Xach 01"
                rating="4"
              />
            </div>
          );
        })}
      </>
    );
  };

  render() {
    return (
      <div className="section">
        <div className="container">
          <div className="row">
            {this.renderAside()}
            {this.renderMain()}
          </div>
        </div>
      </div>
    );
  }
}

export default Products;
