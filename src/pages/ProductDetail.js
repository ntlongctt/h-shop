import React, { Component } from "react";
import CustomSlider from "../components/Slider";
import { NewProduct, ProductSale } from "../components/ProductStatus";
import RatingStart from "../components/RatingStart";
import ProductSizeSelect from '../components/ProductSizeSelect';
import ProductColorSelect from '../components/ProductColorSelect'

class ProductDetail extends Component {
  state = {
    nav1: null,
    nav2: null
  };

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  ProductPreview = () => {
    const productImgs = [
      <div className="product-view">
        <img src="https://i.ibb.co/YBCpJ1G/main-product01.jpg" alt="" />
      </div>,
      <div className="product-view">
        <img src="https://i.ibb.co/rvfJfFd/main-product02.jpg" alt="" />
      </div>,
      <div className="product-view">
        <img src="https://i.ibb.co/bRkqdHK/main-product03.jpg" alt="" />
      </div>,
      <div className="product-view">
        <img src="https://i.ibb.co/3TyGXpF/main-product04.jpg" alt="" />
      </div>
    ];

    const productImgsThumb = [
      <div className="product-view thumbs">
        <img src="https://i.ibb.co/qk7LJ4r/thumb-product01.jpg" alt="" />
      </div>,
      <div className="product-view thumbs">
        <img src="https://i.ibb.co/Vm04F1q/thumb-product02.jpg" alt="" />
      </div>,
      <div className="product-view thumbs">
        <img src="https://i.ibb.co/25g6wQS/thumb-product03.jpg" alt="" />
      </div>,
      <div className="product-view thumbs">
        <img src="https://i.ibb.co/q5Sy10C/thumb-product04.jpg" alt="" />
      </div>
    ];

    const mainViewSetting = {
      infinite: true,
      speed: 300,
      dots: false,
      arrows: true,
      fade: true,
      autoplay: false
      // asNavFor: {this.state.},
    };

    const thumbsViewSetting = {
      slidesToShow: 3,
      slidesToScroll: 1,
      arrows: true,
      centerMode: true,
      focusOnSelect: true,
      dots: false,
      autoplay: false
    };
    return (
      <div>
        <div>
          <CustomSlider
            asNavFor={this.state.nav2}
            ref={slider => (this.slider1 = slider)}
            slides={productImgs}
            setting={mainViewSetting}
            size={585}
          />
        </div>
        <div>
          <CustomSlider
            asNavFor={this.state.nav1}
            ref={slider => (this.slider2 = slider)}
            slides={productImgsThumb}
            setting={thumbsViewSetting}
            size={555}
          />
        </div>
      </div>
    );
  };
  onChangeSie = (s) => {
    console.log(s);
    
  }
  ProductOption = () => {
    return (
      <div className="product-options">
        <ProductSizeSelect sizes={['s', 'xl', 'sl']} defaultSize='xl' onChangeSize={this.onChangeSie}/>
        <ProductColorSelect />
        {/* <ul className="color-option">
          <li>
            <span className="text-uppercase">Color:</span>
          </li>
          <li className="active">
            <a href="/url" style="background-color:#475984;" />
          </li>
          <li>
            <a href="/url" style="background-color:#8A2454;" />
          </li>
          <li>
            <a href="/url" style="background-color:#BF6989;" />
          </li>
          <li>
            <a href="/url" style="background-color:#9A54D8;" />
          </li>
        </ul> */}
      </div>
    );
  };

  renderProductBody = () => {
    return (
      <div className="product-body">
        <div className="product-label">
          <NewProduct />
          <ProductSale percent="20" />
        </div>
        <h2 className="product-name">Product Name Goes Here</h2>
        <h3 className="product-price">
          $32.50 <del className="product-old-price">$45.00</del>
        </h3>
        <div className="d-flex">
          <RatingStart rating={3} size={16} />
          <a className="pl-20" href="#">
            3 Review(s) / Add Review
          </a>
        </div>
        <p>
          <strong>Availability:</strong> In Stock
        </p>
        <p className="font-14">
          Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris nisi ut
          aliquip ex ea commodo consequat. Duis aute irure dolor in
          reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla
          pariatur. Excepteur sint occaecat cupidatat non proident, sunt in
          culpa qui officia deserunt mollit anim id est laborum.
        </p>
        {this.ProductOption()}
      </div>
    );
  };

  render() {
    return (
      <div className="section">
        <div className="container">
          <div className="row">
            <div className="col-md-6">{this.ProductPreview()}</div>
            <div className="col-md-6">{this.renderProductBody()}</div>
            <div className="col-md-12">3</div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProductDetail;
