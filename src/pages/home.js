import React, { Component } from "react";
import CustomSlider from "../components/Slider";
import Header from "../components/Header";
import Menu from "../components/Menu";
import ProductCard from "../components/ProductCard";
import Banner from '../components/Banner';

import Banner10 from "../assets/images/banner10.jpg";
import Banner11 from "../assets/images/banner11.jpg";
import Banner12 from "../assets/images/banner12.jpg";
import Banner14 from "../assets/images/banner14.jpg";

import product01 from "../assets/images/product01.jpg";
import product07 from "../assets/images/product07.jpg";
import product06 from "../assets/images/product06.jpg";
import product08 from "../assets/images/product08.jpg";

import Products from './Products';
import ProductDetail from './ProductDetail';

class Home extends Component {
  state = {
    contacts: ["Harry", "Ron", "Hermoine", "Hagrid", "Hedwig"],
    count: 0,
    show: false
  };

  show = () => {
    this.setState({
      show: true
    });
  };

  handleImageLick = image => {
    console.log(image);
  };

  rednderNewColecction = () => (
    <div className="section">
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-sm-6">
            <a className="banner banner-1" href="/url">
              <img src={Banner10} alt="" />
              <div className="banner-caption text-center">
                <h2 className="white-color">NEW COLLECTION</h2>
              </div>
            </a>
          </div>

          <div className="col-md-4 col-sm-6">
            <a className="banner banner-1" href="/url">
              <img src={Banner11} alt="" />
              <div className="banner-caption text-center">
                <h2 className="white-color">NEW COLLECTION</h2>
              </div>
            </a>
          </div>

          <div className="col-md-4 col-md-offset-0 col-sm-6 col-sm-offset-3">
            <a className="banner banner-1" href="/url">
              <img src={Banner12} alt="" />
              <div className="banner-caption text-center">
                <h2 className="white-color">NEW COLLECTION</h2>
              </div>
            </a>
          </div>
        </div>
      </div>
    </div>
  );

  rednerDealOfTheDay = () => {
    const productCards = [
      <ProductCard
        isNew={false}
        // discountPercent="30"
        // countdownEnd="2019-06-23T10:13:17.800Z"
        productImg="https://i.ibb.co/rpDDkPs/product01.jpg"
        // discountPrice="32.50"
        originalPrice="45.00"
        productUrl="/home"
        productName="Tui Xach 01"
        rating='3'
      />,
      <ProductCard
        isNew={true}
        discountPercent="30"
        countdownEnd="2019-06-25T10:13:17.800Z"
        productImg="https://i.ibb.co/nCNntgJ/product02.jpg"
        discountPrice="32.50"
        originalPrice="45.00"
        productUrl="/home"
        productName="Tui Xach 01"
        rating='4'
      />,
      <ProductCard
        isNew={true}
        discountPercent="30"
        countdownEnd="2019-06-23T16:13:17.800Z"
        productImg="https://i.ibb.co/Wc6Tx7J/product06.jpg"
        discountPrice="32.50"
        originalPrice="45.00"
        productUrl="/home"
        productName="Tui Xach 01"
        rating='5'
      />,
      <ProductCard
        isNew={true}
        discountPercent="30"
        countdownEnd="2019-06-27T12:13:17.800Z"
        productImg="https://i.ibb.co/hm9wc06/product03.jpg"
        discountPrice="32.50"
        originalPrice="45.00"
        productUrl="/home"
        productName="Tui Xach 01"
      />
    ];

    const productsSettings = {
      slidesToShow: 3,
      slidesToScroll: 2,
      autoplay: false,
      infinite: true,
      speed: 300,
      dots: false,
      arrows: false,
      responsive: [
        {
          breakpoint: 991,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        },
        {
          breakpoint: 480,
          settings: {
            dots: false,
            arrows: true,
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };
    return (
      <div className="section">
        <div className="container">
          <div className="row">
            <div className="col-md-12">
              <div className="section-title">
                <h2 className="title">Deals Of The Day</h2>
                <div className="pull-right">
                  <div className="product-slick-dots-1 custom-dots" />
                </div>
              </div>
            </div>

            <div className="col-md-3 col-sm-6 col-xs-6">
              <div className="banner banner-2">
                <img src={Banner14} alt="" />
                <div className="banner-caption">
                  <h2 className="white-color">
                    NEW
                    <br />
                    COLLECTION
                  </h2>
                  <button className="primary-btn">Shop Now</button>
                </div>
              </div>
            </div>

            <div className="col-md-9 col-sm-6 col-xs-6">
              <div className="row">
                <div id="product-slick-1" className="">
                  <CustomSlider
                    size={840}
                    slides={productCards}
                    setting={productsSettings}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  };

  render() {
    

    return (
      <>
        <Header />
        <Menu />

        <ProductDetail />

        {/* <Banner />
        {this.rednderNewColecction()}
        {this.rednerDealOfTheDay()} */}


      </>
    );
  }
}

export default Home;
