import React from 'react';

const styles = {
  position: 'relative',
  display: 'inline-block',
  padding: '5px 15px',
  fontWeight: '700',
  color: "#FFF",
  backgroundColor: '#F8694A',
  zIndex: 22
}

const ProductSale = (props) => (
  <span style={styles}>-{props.percent}%</span>
)

export default ProductSale;