import React from 'react';

const styles = {
  position: 'relative',
  display: 'inline-block',
  padding: '5px 15px',
  fontWeight: '700',
  color: "#FFF",
  backgroundColor: '#30323A',
  zIndex: 22
}

const NewProduct = () => (
  <span style={styles}>New</span>
)

export default NewProduct;