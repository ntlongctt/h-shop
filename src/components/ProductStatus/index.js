import NewProduct from './New';
import ProductSale from './Sale';

export {
  NewProduct,
  ProductSale
}