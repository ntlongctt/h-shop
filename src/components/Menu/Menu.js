/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { Component } from "react";
import { Slide } from "../../assets/animations/slide";
import "./styles.css";
import SubMenu from './SubMenu';

class Menu extends Component {
  state = {
    show: false,
    count: 0
  };

  show = () => {
    if (this.state.show !== true) {
      this.setState({
        show: true,
        count: Date.now()
      });
    }
  };

  hide = () => {
    this.setState({
      show: false,
    })
  }

  SubMenub = () => (
    <div className="menu-custom">
      <div className="row">
        <div className="col-md-4">
          <ul className="list-links">
            <li>
              <h3 className="list-links-title">Categories</h3>
            </li>
            <li>
              <a href="/home">Women’s Clothing</a>
            </li>
            <li>
              <a href="/home">Men’s Clothing</a>
            </li>
            <li>
              <a href="/home">Phones & Accessories</a>
            </li>
            <li>
              <a href="/home">Jewelry & Watches</a>
            </li>
            <li>
              <a href="/home">Bags & Shoes</a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  );

  render() {
    return (
      <div id="navigation">
        <div className="container">
          <div id="responsive-nav">
            <div className="menu-nav">
              <span className="menu-header">
                Menu <i className="fa fa-bars" />
              </span>
              <ul className="menu-list">
                <li>
                  <a href="/home">Home</a>
                </li>
                <li>
                  <a href="/home">Shop</a>
                </li>
                {/* <li className="dropdown mega-dropdown" onMouseLeave={this.hide}>
                  <a
                    className="dropdown-toggle"
                    data-toggle="dropdown"
                    aria-expanded="true"
                    onMouseEnter={this.show}
                  >
                    Women <i className="fa fa-caret-down" />
                  </a>
                  <Slide show={this.state.show} id={this.state.count}>
                    {this.SubMenu()}
                  </Slide>
                </li> */}
                <SubMenu ></SubMenu>
                <SubMenu ></SubMenu>
              </ul>
            </div>
          </div>
        </div>
        {/* <button onClick={this.show}>show</button> */}
      </div>
    );
  }
}

export default Menu;
