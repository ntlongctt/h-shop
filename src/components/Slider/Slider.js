import React, { Component } from "react";
import Slider from "react-slick";
import "./styles.css";

const _defauleSetting = {
  speed: 500,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
  arrows: true
};

const _defaultStyleSize = "700px";

class CustomSlider extends Component {
  render() {
    const styles = {
      size: {
        width: this.props.size ? this.props.size : _defaultStyleSize
      }
    };
    const setting = this.props.setting
      ? Object.assign({..._defauleSetting}, this.props.setting)
      : {..._defauleSetting};
    const slides = this.props.slides;
    console.log(this.props.setting, setting);
    
    
    return (
      <div className='slider-layout'>
        <div style={styles.size}>
          <Slider {...setting}>
            {slides.map(slide => (
              <div key={new Date().getTime()}>
                {slide}
              </div>
            ))}
          </Slider>
        </div>
      </div>
    );
  }
}

export default CustomSlider;
