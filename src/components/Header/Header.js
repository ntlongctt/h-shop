import React, { Component } from "react";
import "./styles.css";
// import '../../../node_modules/bootstrap/dist/css/bootstrap.min.css'
import {ShoppingCart, SVGSearch} from '../../assets/icons';
import logo from "../../assets/images/logo.png";


class Header extends Component {
  HeaderLeft = () => (
    <div className="pull-left">
      <div className="header-logo">
        <a className="logo" href="/home">
          <img src={logo} alt="" />
        </a>
      </div>
      <div className="header-search">
        <form>
          <input
            className="input search-input"
            type="text"
            placeholder="Enter your keyword"
          />
          <button className="search-btn">
            <SVGSearch size='22'/>
          </button>
        </form>
      </div>
    </div>
  );

  HeaderRight = () => (
    <div className="pull-right">
      <ul className="header-btns">
        <li className="header-cart dropdown default-dropdown">
          <a
            className="dropdown-toggle"
            data-toggle="dropdown"
            aria-expanded="true"
            href="/home"
          >
            <div className="header-btns-icon">
              {/* <i className="fa fa-shopping-cart" /> */}
              <ShoppingCart width='32'/>
              <span className="qty">3</span>
            </div>
            <strong className="text-uppercase">My Cart:</strong>
            <br />
            <span>35.20$</span>
          </a>
          {/* <div className="custom-menu">
            <div id="shopping-cart">
              <div className="shopping-cart-list">
                <div className="product product-widget">
                  <div className="product-thumb">
                    <img src="./img/thumb-product01.jpg" alt="" />
                  </div>
                  <div className="product-body">
                    <h3 className="product-price">
                      $32.50 <span className="qty">x3</span>
                    </h3>
                    <h2 className="product-name">
                      <a href="#">Product Name Goes Here</a>
                    </h2>
                  </div>
                  <button className="cancel-btn">
                    <i className="fa fa-trash" />
                  </button>
                </div>
                <div className="product product-widget">
                  <div className="product-thumb">
                    <img src="./img/thumb-product01.jpg" alt="" />
                  </div>
                  <div className="product-body">
                    <h3 className="product-price">
                      $32.50 <span className="qty">x3</span>
                    </h3>
                    <h2 className="product-name">
                      <a href="#">Product Name Goes Here</a>
                    </h2>
                  </div>
                  <button className="cancel-btn">
                    <i className="fa fa-trash" />
                  </button>
                </div>
              </div>
              <div className="shopping-cart-btns">
                <button className="main-btn">View Cart</button>
                <button className="primary-btn">
                  Checkout <i className="fa fa-arrow-circle-right" />
                </button>
              </div>
            </div>
          </div> */}
        </li>
        {/* <li className="nav-toggle">
          <button className="nav-toggle-btn main-btn icon-btn">
            <i className="fa fa-bars" />
          </button>
        </li> */}
      </ul>
    </div>
  );

  render() {
    return (
      <div>
        <div className="container d-flex justify-content-between">
          {this.HeaderLeft()}
          {this.HeaderRight()}
        </div>
      </div>
    );
  }
}

export default Header;
