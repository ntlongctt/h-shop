import React, { Component } from "react";
import './styles.scss';

class ProductSizeSelect extends Component {

  render() {
    return (
      <ul className="size-option">
        <li>
          <span className="text-uppercase">Size:</span>
        </li>
        {this.props.sizes.map(s => (
          <li key={s} className={this.props.defaultSize === s ? "active" : ""}>
            <p style={styles.size} className="text-uppercase size" href='\url' onClick={() => this.props.onChangeSize(s)}>{s}</p>
          </li>
        ))}
      </ul>
    );
  }
}

const styles = {
  size: {
    display: 'block',
    border: '1px solid #DADADA',
    fontWeight: 700,
    padding: '2px 7px 0px 8px',
  },
}
export default ProductSizeSelect;
