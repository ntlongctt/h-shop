/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/anchor-has-content */
import React, { Component } from 'react';

class ProductColorSelect extends Component {

  state = {
    selectedColor: ''
  }
  
  colors = [
    '#8A2454',
    '#BF6989',
    '#9A54D8',
    '#475984'
  ]

  getStyle = (color) => {
    return {
      background: color,
      boxShadow: this.state.selectedColor === color ? '0px 0px 0px 2px #FFF, 0px 0px 0px 3px #F8694A' : '',
      // margin: 2
    };
  }

  handleSelect = (color) => {
    this.setState({
      selectedColor: color
    })
  }

  render() {
    return (
      <ul className="color-option">
        <li><span className="text-uppercase">Color:</span></li>
        {
          this.colors.map((color) => (
            <li key={color}>
              <a style={this.getStyle(color)} onClick={() => this.handleSelect(color)}></a>
            </li>
          ))
        }
      </ul>
    );
  }
}

export default ProductColorSelect;