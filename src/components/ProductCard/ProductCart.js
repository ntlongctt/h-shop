import React, { useState } from "react";
import moment from "moment";
import "./styles.css";
import {SVGStart} from '../../assets/icons'
import RatingStart from '../RatingStart';

const ProductCart = ({
  isNew,
  discountPercent,
  countdownEnd,
  productImg,
  discountPrice,
  originalPrice,
  productUrl,
  productName,
  rating
}) => {
  const [h, setH] = useState(0);
  const [m, setM] = useState(0);
  const [s, setS] = useState(0);

  // update coundown
  if (countdownEnd) {
    setInterval(() => {
      const duration = moment.duration(moment(countdownEnd).diff(moment()));
      const hours = Math.floor(duration.asHours());
      setH(hours < 10 ? "0" + hours : hours);
      setM(
        duration.minutes() < 10 ? "0" + duration.minutes() : duration.minutes()
      );
      setS(
        duration.seconds() < 10 ? "0" + duration.seconds() : duration.seconds()
      );
    }, 1000);
  }

  return (
    <div className="product product-single">
      <div className="product-thumb">
        <div className="product-label">
          {isNew && <span>New</span>}
          {discountPercent && <span className="sale">-{discountPercent}%</span>}
        </div>
        {countdownEnd && (
          <ul className="product-countdown">
            <li>
              <span>{h} H</span>
            </li>
            <li>
              <span>{m} M</span>
            </li>
            <li>
              <span>{s} S</span>
            </li>
          </ul>
        )}

        <button className="main-btn quick-view">
          <i className="fa fa-search-plus" /> Quick view
        </button>
        <img src={productImg} alt="" />
      </div>
      <div className="product-body">
        <h3 className="product-price">
          ${discountPrice}{" "}
          <del className="product-old-price">${originalPrice}</del>
        </h3>
        <div className="product-rating">
          {/* {
            Array.from(Array(5)).map((i, idx) => (
              <SVGStart key={idx} empty={(idx >= rating) || !rating} size='12'/>
            ))
          } */}
          <RatingStart rating={rating} />
        </div>
        <h2 className="product-name">
          <a className="non-link" href={productUrl}>
            {productName}
          </a>
        </h2>
        <div className="product-btns">
          {/* <button className="main-btn icon-btn">
            <i className="fa fa-heart" />
          </button>
          <button className="main-btn icon-btn">
            <i className="fa fa-exchange" />
          </button> */}
          <button className="primary-btn add-to-cart w-full">
            <i className="fa fa-shopping-cart" /> Add to Cart
          </button>
        </div>
      </div>
    </div>
  );
};

export default ProductCart;
