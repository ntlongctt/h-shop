import React, { Component } from 'react';
import CustomSlider from '../../components/Slider';

class Banner extends Component {
  render() {
    const images = [
      <div className="banner banner-1">
        <img
          src={"https://i.ibb.co/WysfdW4/banner02.jpg"}
          alt=""
          onClick={() => this.handleImageLick("banner02")}
        />
      </div>,
      <div className="banner banner-1">
        <img
          src={"https://i.ibb.co/vBGm8cp/banner01.jpg"}
          alt=""
          onClick={() => this.handleImageLick("banner02")}
        />
      </div>,
      <div className="banner banner-1">
        <img
          src={"https://i.ibb.co/LzBYdyH/banner03.jpg"}
          alt=""
          onClick={() => this.handleImageLick("banner02")}
        />
      </div>,
    ];
    return (
      <div className="container">
        <CustomSlider slides={images} setting={{ dots: false }} />
      </div>
    );
  }
}

export default Banner;