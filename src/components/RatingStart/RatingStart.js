import React from "react";
import { SVGStart } from "../../assets/icons";

const RatingStart = props => (
  <div>
    {Array.from(Array(5)).map((i, idx) => (
      <SVGStart
        key={idx}
        empty={idx >= props.rating || !props.rating}
        size={props.size || "12"}
      />
    ))}
  </div>
);

export default RatingStart;
