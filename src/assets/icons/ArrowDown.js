import React from "react";
const SVGArrowDown = props => {
  const rotation = {
    up: "0deg",
    right: "90deg",
    down: "180deg",
    left: "270deg"
  };
  const color = "#1E201D";
  const style = {
    enableBackground: "new 0 0 31.479 31.479",
    transform: `rotate(${rotation[props.direction]})`,
    transformOrigin: "center center"
  };

  console.log(rotation[props.direction]);
  

  return (
    <svg
      x="0px"
      y="0px"
      viewBox="0 0 31.479 31.479"
      width={props.size}
      style={style}
    >
      <path
        fill={color}
        d="M26.477,10.274c0.444,0.444,0.444,1.143,0,1.587c-0.429,0.429-1.143,0.429-1.571,0l-8.047-8.047
	v26.555c0,0.619-0.492,1.111-1.111,1.111c-0.619,0-1.127-0.492-1.127-1.111V3.813l-8.031,8.047c-0.444,0.429-1.159,0.429-1.587,0
	c-0.444-0.444-0.444-1.143,0-1.587l9.952-9.952c0.429-0.429,1.143-0.429,1.571,0L26.477,10.274z"
      />
    </svg>
  );
};

export default SVGArrowDown;
