import ShoppingCart from './ShoppingCart'
import SVGSearch from './Search'
import SVGHeart from './Heart'
import SVGStart from './Start'
import SVGArrowDown from './ArrowDown'

export {
  ShoppingCart,
  SVGSearch,
  SVGHeart,
  SVGStart,
  SVGArrowDown
}