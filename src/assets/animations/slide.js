import React from "react";
import { CSSTransition, TransitionGroup } from "react-transition-group";
import './style.css';

export const Slide = ({ id, show, children }) => {
  return  (
    <TransitionGroup component={null}>
      <CSSTransition
        in={true}
        appear={false}
        key={id}
        timeout={900}
        classNames="slide"
      >
        {show === true ? children : (<></>)}
      </CSSTransition>
    </TransitionGroup>
  )
};
